package com.fanda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FavoriteDAO extends JpaRepository<Favorite, Integer>{

}
