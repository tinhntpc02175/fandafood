package com.fanda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodDAO extends JpaRepository<Food, Integer>{

}
