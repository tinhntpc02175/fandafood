package com.fanda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PrioritizedDAO extends JpaRepository<Prioritized, Integer>{

}
