package com.fanda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PictureDAO extends JpaRepository<Picture, Integer>{

}
