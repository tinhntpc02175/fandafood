package com.fanda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PremiumBundleDAO extends JpaRepository<PremiumBundle, Integer>{

}
