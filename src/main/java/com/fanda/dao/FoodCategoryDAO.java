package com.fanda.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodCategoryDAO extends JpaRepository<FoodCategory, Integer>{

}
